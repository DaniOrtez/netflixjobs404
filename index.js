const hamburguer = document.getElementById('toAnimate')
const closeMenu = document.getElementById('closeMenu')
const firstContainer = document.getElementById('first')
const secondContainer = document.getElementById('second')
const thirdContainer = document.getElementById('third')

function animation() {
  let pos = 100;
  const firstExecute = setInterval(frame('first'), 16)
  const secondExecute = setInterval(frame('second'), 16)
  const thirdExecute = setInterval(frame('third'), 16)
  function frame(position) {
    return function () {
      switch (position) {
        case 'first':
          if (pos === 40) {
            clearInterval(firstExecute)
          } else {
            pos--
            firstContainer.style.left = `${pos}%`
          }
          break;

        case 'second':
          if (pos === 45) {
            clearInterval(secondExecute)
          } else {
            pos--
            secondContainer.style.left = `${pos}%`
          }
          break;

        case 'third':
          if (pos === 50) {
            closeMenu.style.display = 'block'
            clearInterval(thirdExecute)
          } else {
            pos--
            thirdContainer.style.left = `${pos}%`
          }
          break;
      
        default:
          break;
      }
    }
  }
}

hamburguer.addEventListener('click', () => {
  thirdContainer.style.opacity = 1
  animation()
})

closeMenu.addEventListener('click', () => {
  [firstContainer, secondContainer, thirdContainer].forEach(container => {
    container.style.left = '100%'
  })
  closeMenu.style.display = 'none'
  thirdContainer.style.opacity = 0
})